# Opciones
CC=gcc 
CFLAGS = -g -Wall
CFLAGS_M = -shared

# Listas de objetos
SRCS = ${wildcard src/*.c}
M_SRCS = ${wildcard ext/*.c}
OBJS = ${patsubst %.c, %.o, ${SRCS}}
M_OBJS = ${patsubst %.c, %.o, ${M_SRCS}}
M_DLLS = ${patsubst %.c, %.so, ${M_SRCS}}

#Reglas
all: cosa 

cosa: ${OBJS} ${M_OBJS}
	${CC} ${CFLAGS} ${OBJS} -o $@
		
dependencias: ${SRCS}
	${CC} -MM ${SRCS} > $@

modulos: ${M_DLLS}

ext/%.so: ext/%.o
	${CC} ${CFLAGS_M} -o $@ $<
				
clean:
	rm cosa ${OBJS} ${M_DLLS} ${M_OBJS}

include dependencias
