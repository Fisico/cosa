#ifndef cartaH
   #include "carta.h"
   #define cartaH
#endif

struct mazo{
	struct carta* carta;
	struct mazo* siguiente;
};

int Mazo_crear( struct mazo** m );

int Mazo_ponerCartaArriba( struct mazo** m, struct carta* c );
	
int Mazo_ponerCartaAbajo( struct mazo** m, struct carta* c );

int Mazo_obtenerNumeroCartas( struct mazo* m );

int Mazo_numCartasTipo( struct mazo* m , int tipo );

int Mazo_barajar( struct mazo** m );
	
int Mazo_robarCarta( struct mazo** m, struct carta** c );

int Mazo_robarCartaMedio( struct mazo** m, int numeroCarta, struct carta** c );

int Mazo_agregarMazo( struct mazo** m, struct mazo* otro );

int Mazo_vacio( struct mazo** m );

int Mazo_destruir( struct mazo** m );	
