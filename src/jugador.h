#ifndef mazoH
   #include "mazo.h"
   #define mazoH
#endif

#ifndef mesaH
   #include "mesa.h"
   #define mesaH
#endif

#define NUM_CARTAS_MANO 6

struct jugador {
	struct mazo* mano;
	int bando;
	int vivo;
	int humano;
	char nombre[100];
};

int Jugador_crear( struct jugador** j, int humano, int nombre );

int Jugador_descartarAlAzar( struct jugador** j, struct carta** c );

int Jugador_descartarConsciente( struct jugador** j, struct carta** c , int motivo );

int Jugador_cogerCarta( struct jugador** j, struct carta* c );

int Jugador_numeroCartasMano( struct jugador* j );

int Jugador_robar( struct jugador** j , struct mesa** m );

int Jugador_destruir( struct jugador** j );