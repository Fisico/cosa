#ifndef mesaH
   #include "mesa.h"
   #define mesaH
#endif

#ifndef jugadorH
   #include "jugador.h"
   #define jugadorH
#endif

#define NUM_JUGADORES 6

#define MOTIVO_FALLO_MISION 1
#define MOTIVO_EXITO_MISION 2
#define MOTIVO_CONVERSACION 3
#define MOTIVO_REVISAR			4
#define MOTIVO_ATACAR				5
#define MOTIVO_HERIDA				6
#define MOTIVO_SOSPECHA			7
#define MOTIVO_SOSPECHOSO		8

#define TEXTO_FALLO_MISION "Que carta descartar por fallo al coordinar la mision?"

struct partida {
	struct mesa* mesa;
	struct jugador* jugadores[NUM_JUGADORES];
};

int Partida_crear( struct partida** p );

int Partida_preparar( struct partida** p );

int Partida_comenzar( struct partida** p );

int Partida_final( struct partida * p );

int Partida_juegaRonda( struct partida ** p, struct jugador ** j );

int Partida_mision( struct partida ** p, struct jugador ** j );

int Partida_revisar( struct partida ** p, struct jugador ** j );

int Partida_sospechar( struct partida ** p, struct jugador ** j );

int Partida_atacar( struct partida ** p, struct jugador ** j );

int Partida_pasar( struct partida ** p, struct jugador ** j );

int Partida_destruir( struct partida** p );