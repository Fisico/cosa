#ifndef mazoH
   #include "mazo.h"
   #define mazoH
#endif

#define NUM_MISIONES 6

struct mesa {
	struct mazo* robo;
	struct mazo* descartes;
	struct carta misiones[NUM_MISIONES];
	struct mazo* mazoMisiones[NUM_MISIONES];
};

int Mesa_crear( struct mesa** m );

int Mesa_robarCarta( struct mesa** m, struct carta** c );

int Mesa_descartarCarta( struct mesa** m, struct carta* c );

int Mesa_colaborarMision( struct mesa** m, int mision, struct carta* c );

int Mesa_ponerMazoRobo( struct mesa** m, struct mazo* mazo );

int Mesa_barajarDescartes( struct mesa** m );

int Mesa_destruir( struct mesa** m );