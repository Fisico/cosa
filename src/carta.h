#define TIPO_MECANICA  				1
#define TIPO_INVESTIGACION  	2
#define TIPO_COSA  						3
#define TIPO_SOSPECHA  				4
#define TIPO_PRUEBA  					5
#define TIPO_MISION_MECANICA  6
#define TIPO_MISION_INVESTIGACION  7
#define TIPO_MISION_COSA  		8
#define TIPO_MECANIC0  				9
#define TIPO_MEDICO  					10
#define TIPO_XENOINGENIERO  	11
#define TIPO_MANITAS  				12

struct carta {
	int tipo;
	int valor;
};

int Carta_crear( struct carta** c, int tipo, int valor );
