#include <stdlib.h>
#include <stdio.h>

#ifndef mazoH
   #include "mazo.h"
   #define mazoH
#endif

int Mazo_crear( struct mazo** m ){	
	
	*m = NULL;
	
	return 0;
} 

int Mazo_ponerCartaArriba( struct mazo** m, struct carta* c ) {
	struct mazo* nueva;
	
	nueva = (struct mazo *)malloc( sizeof(struct mazo) );
	if( nueva == NULL ) return -1;
	nueva->siguiente = *m;
	nueva->carta = c;
	*m = nueva;
	
	return 0;
}
	
int Mazo_ponerCartaAbajo( struct mazo** m, struct carta* c ){
	struct mazo* aux;
	struct mazo* nueva;
	
	if( ! Mazo_vacio( m )  ) {
		aux = *m;
		while( aux->siguiente != NULL ) {
			aux = aux->siguiente;
		}
		nueva = (struct mazo *)malloc( sizeof(struct mazo) );
		if( nueva == NULL ) return -1;
		nueva->siguiente = NULL;
		nueva->carta = c;
		aux->siguiente = nueva;	
	} else {
		Mazo_ponerCartaArriba( m , c );
	}	
	
	return 0;
}

int Mazo_obtenerNumeroCartas( struct mazo* m ) {
	struct mazo* aux;
	int numCartas;
	
	numCartas = 0;
	if( !Mazo_vacio( &m )  ) {
		aux = m;
		while( aux != NULL ) {
			aux = aux->siguiente;
			++numCartas;
		}
	}
	
	return numCartas;
}

int Mazo_numCartasTipo( struct mazo* m , int tipo ) {
	struct mazo* aux;
	int numCartas;
	
	numCartas = 0;
	if( !Mazo_vacio( &m )  ) {
		aux = m;
		while( aux != NULL ) {
			if( aux->carta->tipo == tipo ) {
				++numCartas;
			}
			aux = aux->siguiente;
		}
	}
	
	return numCartas;
}
	
int Mazo_barajar( struct mazo** m ){
	struct mazo* nuevo;
	struct carta* aux;
	int cartas;
	int carta;
	int error;
	
	error = 0;
	if( ! Mazo_vacio( m )  ) {
		Mazo_crear( &nuevo );
		cartas = Mazo_obtenerNumeroCartas( *m );
		while( ! Mazo_vacio( m ) ) {
			carta = rand() % cartas ;
			error = Mazo_robarCartaMedio( m , carta, &aux );
			if( error == 0 ) {
				error = Mazo_ponerCartaArriba( &nuevo, aux );
			}
			--cartas;
		}
		Mazo_destruir( m );
		*m = nuevo;
	}
	
	return error;
}
	
int Mazo_robarCarta( struct mazo** m, struct carta** c ) {
	struct mazo* aux;
	
	if( Mazo_vacio( m ) ) return -1;
	aux = *m;
	*c = aux->carta;
	*m = aux->siguiente;
	free( aux );

	return 0;
}

int Mazo_robarCartaMedio( struct mazo** m, int numeroCarta, struct carta** c ) {
	struct mazo* aux;
	struct mazo* aux2;
	
	if( Mazo_vacio( m ) ) return -1;
	if( numeroCarta == 0 ) return Mazo_robarCarta( m, c );
	aux = *m;
	while( aux->siguiente != NULL && numeroCarta > 1 ) {
		aux = aux->siguiente;
		--numeroCarta;
	}
	if( aux->siguiente != NULL ) {
		aux2 = aux->siguiente;
		*c = aux2->carta;
		aux->siguiente = aux2->siguiente;
		free( aux2 );
		return 0;
	}

	return -1;
}

int Mazo_agregarMazo( struct mazo** m, struct mazo* otro ) {
	struct mazo* aux;
	
	if( ! Mazo_vacio( m )  ) {
		aux = *m;
		while( aux->siguiente != NULL ) {
			aux = aux->siguiente;
		}
		aux->siguiente = otro;
	} else {
		*m = otro;
	}
	
	return 0;
}

int Mazo_destruir(struct mazo** m ) {
	struct carta* c;
	
	while( ! Mazo_robarCarta( m, &c ) ) {
		if( c ) free( c );
	}
	if( *m ) free( *m );
	
	return 0;
}

int Mazo_vacio(struct mazo** m) {
	return (*m == NULL );
}

