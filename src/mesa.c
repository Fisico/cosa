#include <stdlib.h>

#ifndef mesaH
   #include "mesa.h"
   #define mesaH
#endif

int Mesa_crear( struct mesa** m ) {
	int error;
	int i;
	struct carta* c;
	struct mazo* aux;
	
	error = 0;
	*m = (struct mesa *)malloc( sizeof(struct mesa) );
	if( *m == NULL ) return -1;
	Mazo_crear( &aux );
	for( i = 0 ; i < NUM_MISIONES ; ++i ) {
		switch( i % 3 ) {
			case 0:
				error = Carta_crear( &c , TIPO_MISION_MECANICA, 0 );
				break;
			case 1:
				error = Carta_crear( &c , TIPO_MISION_INVESTIGACION, 0 );
				break;
			case 2:
				error = Carta_crear( &c , TIPO_MISION_COSA, 0 );
				break;
		}
		if( error == 0 ) {	
			error = Mazo_ponerCartaArriba( &aux , c );
		}		
		if( error == 0 ) {	
			Mazo_crear( &((*m)->mazoMisiones[i]) );
		}
	}
	if( error == 0 ) {
		error = Mazo_barajar( &aux );
	}
	if( error == 0 ) {
		for( i = 0 ; i <NUM_MISIONES ; ++i ) {
			if( error == 0 ) {
				error = Mazo_robarCarta( &aux , &c );
			}
			if( error == 0 ) {
				(*m)->misiones[i].tipo = c->tipo;
				(*m)->misiones[i].valor = c->valor;
				free( c );
			}
		}
	}
	if( aux ) Mazo_destruir( &aux );
	if( error == 0 ) {
		error = Mazo_crear( &((*m)->descartes) );
	}
	if( error == 0 ) {
		error = Mazo_crear( &((*m)->robo) );
	}
		
	return error;
}

int Mesa_consultarMision( struct mesa* m, int mision, struct carta** c ) {
	if( mision < 0 && mision >= NUM_MISIONES ) return -1;
	*c = &(m->misiones[mision]);
	
	return 0;
}

int Mesa_obtenerMazoMision( struct mesa* m, int mision, struct mazo*** mz ) {
	struct mazo* aux;
	
	if( mision < 0 && mision >= NUM_MISIONES ) return -1;
	aux = m->mazoMisiones[mision];
		
	*mz = &(m->mazoMisiones[mision]);
	
	return 0;	
}

int Mesa_robarCarta( struct mesa** m, struct carta** c ) {
	int error;
	
	if( Mazo_vacio( &((*m)->robo) ) ) { 
		Mesa_barajarDescartes( m );
	}
	error = Mazo_robarCarta( &((*m)->robo), c);
	 	
	return error; 
}

int Mesa_descartarCarta( struct mesa** m, struct carta* c ) {
	return Mazo_ponerCartaArriba( &((*m)->descartes) , c );
}

int Mesa_colaborarMision( struct mesa** m, int mision, struct carta* c ) {
	struct mazo** aux;
	int error;
	
	error = Mesa_obtenerMazoMision( *m , mision, &aux );
	if( error == 0 ) {
		error = Mazo_ponerCartaArriba( aux , c );
	}
	if( error == 0 ) {
		error = Mazo_barajar( aux );
	}
	
	return error;
}

int Mesa_ponerMazoRobo( struct mesa** m, struct mazo* mazo ) {
	return Mazo_agregarMazo( &((*m)->robo), mazo);
}

int Mesa_barajarDescartes( struct mesa** m ) {
	struct mazo * aux;
	int error;
	
	aux = (*m)->robo;
	(*m)->robo = (*m)->descartes;
	(*m)->descartes = aux;
	error = Mazo_barajar(&((*m)->robo));
	
	return error;
}

int Mesa_destruir( struct mesa** m ) {
	int i;
	struct mazo* aux;
	
	Mazo_destruir( &((*m)->robo) );
	Mazo_destruir( &((*m)->descartes) );
	for( i = 0 ; i < NUM_MISIONES ; ++i ) {
		if( &((*m)->misiones[i]) ) free( &((*m)->misiones[i]) );
		aux = (*m)->mazoMisiones[i];
		Mazo_destruir( &aux );
	}
	if( (*m)->misiones ) free( (*m)->misiones );
	if( (*m)->mazoMisiones ) free( (*m)->mazoMisiones );
	if( *m ) free( *m );
	
	return 0;
}

/*
int main() {
	struct mesa* m;
	struct carta* c;
	struct mazo** mz;
	int error;
	int i;
	
	srand(time(NULL));
	
	
	consultarMision( m , 2 , &c );
	imprimirCarta( c );
	printf("\n");
	error = obtenerMazoMision( m , 2 , &mz );
	printf( "error %d\n", error);
	imprimirMazo( m->mazoMisiones[2] );
	printf( "direccion original %p\n", &(m->mazoMisiones[2]) );
	printf( "direccion obtenida %p\n", mz );
	printf( "error %d\n", error);
	crearCarta( &c , TIPO_MECANICA , 4 );
	
	ponerCartaArriba( &(m->mazoMisiones[2]), c );
	imprimirMazo( m->mazoMisiones[2] );
	obtenerMazoMision( m , 2 , &mz );
	crearCarta( &c , TIPO_MECANICA , 6 );
	printf( "direccion original %p\n", &(m->mazoMisiones[2]) );
	printf( "direccion obtenida %p\n", mz );
	ponerCartaArriba( mz , c );
	imprimirMazo( m->mazoMisiones[2] );
	printf( "direccion original %p\n", &(m->mazoMisiones[2]) );
	printf( "direccion obtenida %p\n", mz );
	
	
	ponerCartaArriba( &mz, c );
	imprimirMazo( mz );
	obtenerMazoMision( m , 2 , &mz );
	imprimirMazo( mz );
	colaborarMision( &m , 2 , c );
	obtenerMazoMision( m , 2 , &mz );
	imprimirMazo( mz );
	
	destruirMesa( &m );	
	exit(0);
}
*/