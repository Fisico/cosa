#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#ifndef partidaH
   #include "partida.h"
   #define partidaH
#endif

#ifndef iuH
   #include "iu.h"
   #define iuH
#endif

int nombre[] = { 1, 2, 3, 4 ,5, 6, 7, 8, 9 };

int Partida_crear( struct partida** p ) {
	int i;
	int error;
	
	error = 0;
	*p = (struct partida *)malloc(sizeof(struct partida));
	if( *p == NULL ) return -1;
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		if( error == 0 ) {
			error = Jugador_crear( &((*p)->jugadores[i]), 1 , nombre[i] );
		}
	}
	if( error == 0 ) {
		Mesa_crear( &((*p)->mesa) );	
	}
	
	return error;
}	

int Partida_preparar( struct partida** p ) {
	struct mazo * m;
	struct carta * c;
	int num_cartas;
	int i;
	int j;
	
	num_cartas = NUM_JUGADORES * NUM_CARTAS_MANO;
	Mazo_crear( &m );
	for( j = 0 ; j < 2 ; ++j ) {
		for( i = 2 ; i < 10 ; ++i ) {
			Carta_crear( &c , TIPO_SOSPECHA , i );
			Mazo_ponerCartaArriba( &m, c );
			Carta_crear( &c , TIPO_MECANICA , i );
			Mazo_ponerCartaArriba( &m, c );
			Carta_crear( &c , TIPO_INVESTIGACION , i );
			Mazo_ponerCartaArriba( &m, c );
			Carta_crear( &c , TIPO_COSA , i );
			Mazo_ponerCartaArriba( &m, c );
		}
	}
	for( i = 0 ; i < 6 ; ++i ) {
			Carta_crear( &c , TIPO_PRUEBA , 10 );
			Mazo_ponerCartaArriba( &m, c );		
	}
	for( i = 0 ; i < 2 ; ++i ) {
			Carta_crear( &c , TIPO_MECANIC0 , 10 );
			Mazo_ponerCartaArriba( &m, c );		
			Carta_crear( &c , TIPO_MEDICO , 10 );
			Mazo_ponerCartaArriba( &m, c );		
			Carta_crear( &c , TIPO_XENOINGENIERO , 10 );
			Mazo_ponerCartaArriba( &m, c );		
			Carta_crear( &c , TIPO_MANITAS , 10 );
			Mazo_ponerCartaArriba( &m, c );		
	}
	Mazo_barajar( &m );
	Mesa_ponerMazoRobo( &(*p)->mesa, m );
	Mazo_crear( &m );
	Carta_crear( &c, TIPO_COSA , 1 );
	Mazo_ponerCartaArriba( &m, c );
	for( i = 1 ; i < NUM_JUGADORES ; ++i ) {
		if( i % 2 == 0 ) {
			Carta_crear( &c, TIPO_MECANICA , 1 );
		} else {
			Carta_crear( &c, TIPO_INVESTIGACION , 1 );
		}
		Mazo_ponerCartaArriba( &m, c );
	}
	Mazo_barajar( &m );
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		Mazo_robarCarta( &m , &c );
		Jugador_cogerCarta( &(*p)->jugadores[i]  , c );
	}
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		Jugador_robar( &(*p)->jugadores[i] , &(*p)->mesa );
	}

	return 0;
}

int Partida_comenzar( struct partida** p ) {
	int i;
	int error;
	
	i = 0;
	while( Partida_final( *p ) == 0 ) {
		if( (*p)->jugadores[i]->vivo == 1 ) {
			error = Partida_juegaRonda( p , &((*p)->jugadores[i]) );
		}
		i = (i + 1)% NUM_JUGADORES;
	}
	
	return error;
}

int Partida_final( struct partida * p ) {
	int res;
	int i;
	
	res = 1;
	for( i = 0 ; i < NUM_MISIONES ; ++i ) {
		if( Mazo_obtenerNumeroCartas( p->mesa->mazoMisiones[i] ) != NUM_JUGADORES ) {
			res = 0;
		}
	}
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		if( p->jugadores[i]->vivo == 1 && p->jugadores[i]->bando == 0 ) {
			res = 0;
		}
	}

	return res;
}

int Partida_juegaRonda( struct partida ** p, struct jugador ** j ) {
	int respuesta;
	int error;
	struct opcion opciones[5];
	int numOpciones;
	
	Jugador_robar( j , &(*p)->mesa );
	Iu_imprimirPartida( *p , 1, *j );
	printf( "Turno de " );
	Iu_imprimirJugador( *j , 1 );
	numOpciones = 0;
	if( Mazo_numCartasTipo( (*j)->mano , TIPO_MECANIC0 ) > 0 || Mazo_numCartasTipo( (*j)->mano , TIPO_MEDICO ) > 0
		  || Mazo_numCartasTipo( (*j)->mano , TIPO_XENOINGENIERO ) > 0 || Mazo_numCartasTipo( (*j)->mano , TIPO_MANITAS ) > 0 ) {
		opciones[numOpciones].codigo = 1;
		strcpy( opciones[numOpciones].texto, "Revisar base" );
		numOpciones++;
	}
	if( (Mazo_numCartasTipo( (*j)->mano , TIPO_MECANICA ) + Mazo_numCartasTipo( (*j)->mano , TIPO_INVESTIGACION ) + Mazo_numCartasTipo( (*j)->mano , TIPO_COSA ) ) > 1 ) {
		opciones[numOpciones].codigo = 2;
		strcpy( opciones[numOpciones].texto, "Atacar a alguien" );	
		numOpciones++;
	}
	if( Mazo_numCartasTipo( (*j)->mano , TIPO_SOSPECHA ) > 0 || Mazo_numCartasTipo( (*j)->mano , TIPO_PRUEBA ) > 0 ) {
		opciones[numOpciones].codigo = 3;
		strcpy( opciones[numOpciones].texto , "Sospechar de alguien" );	
		numOpciones++;
	}
	if( Mazo_obtenerNumeroCartas( (*j)->mano ) > 2 && (Mazo_numCartasTipo( (*j)->mano , TIPO_MECANICA ) + Mazo_numCartasTipo( (*j)->mano , TIPO_INVESTIGACION ) + Mazo_numCartasTipo( (*j)->mano , TIPO_COSA ) ) > 0) {
		opciones[numOpciones].codigo = 4;
		strcpy( opciones[numOpciones].texto , "Realizar mision" );	
		numOpciones++;
	}
	opciones[numOpciones].codigo = 5;
	strcpy( opciones[numOpciones].texto , "Pasar" );
	numOpciones++;
		
	respuesta = Iu_preguntar( "Que accion quieres realizar?" , opciones , numOpciones , (*j)->nombre );
	switch( respuesta ) {
		case 1:
			error = Partida_revisar( p, j );
			break;
		case 2:
			error = Partida_atacar( p, j );
			break;	
		case 3:
			error = Partida_sospechar( p, j );
			break;
		case 4:
			error = Partida_mision( p, j );
			break;
		case 5:
			error = Partida_pasar( p , j );	
	}
	
	return error;
}

int Partida_revisar( struct partida ** p, struct jugador ** j ) {
	int i;
	int mision;
	int res;
	int carta;
	int error;
	int tipo;
	struct carta * c;
	struct opcion opciones[NUM_MISIONES];

	Jugador_descartarConsciente( j , &c , MOTIVO_REVISAR );
	for( i = 0 ; i < NUM_MISIONES ; ++i ) {
		opciones[i].codigo = i + 1;
		sprintf( opciones[i].texto , "Mision %i", i + 1 );
	}
	mision = Iu_preguntar("Que mision quieres revisar?", opciones, NUM_MISIONES, (*j)->nombre ) - 1;
	Iu_imprimirCarta( &((*p)->mesa->misiones[mision]) );
	printf("\n");
	Mazo_barajar( &((*p)->mesa->mazoMisiones[mision]) );
	Iu_imprimirMazo( (*p)->mesa->mazoMisiones[mision] );
	printf("\n");
	tipo = (*p)->mesa->misiones[mision].tipo;
	if( ( ( tipo == TIPO_MISION_MECANICA ) && ( c->tipo == TIPO_MECANIC0 || c->tipo == TIPO_MANITAS ) )
			|| ( ( tipo == TIPO_MISION_INVESTIGACION ) && ( c->tipo == TIPO_MEDICO || c->tipo == TIPO_MANITAS ) )
			|| ( ( tipo == TIPO_MISION_COSA ) && ( c->tipo == TIPO_XENOINGENIERO || c->tipo == TIPO_MANITAS ) ) ) {
		opciones[0].codigo = 1;
		strcpy( opciones[0].texto, "Si" );	
		opciones[1].codigo = 2;
		strcpy( opciones[1].texto , "No" );	
		res = Iu_preguntar( "Quieres descartar alguna carta?" , opciones,  2 , (*j)->nombre );
		if( res == 1 ) {
			carta = Iu_escogerCarta( "Que carta descartar?", ((*p)->mesa)->mazoMisiones[mision] , (*j)->nombre );
			error = Mazo_robarCartaMedio(  &(((*p)->mesa)->mazoMisiones[mision]) , carta - 1, &c );
			Mesa_descartarCarta( &((*p)->mesa), c );
		}
	}
	
	return 0;
}

int Partida_sospechar( struct partida ** p, struct jugador ** j ) {
 	int i;
 	int k;
 	int m;
 	struct opcion opciones[NUM_JUGADORES];
 	struct opcion opciones2[NUM_CARTAS_MANO];
 	struct mazo* maz;
 	struct carta * c;

	Jugador_descartarConsciente( j , &c , MOTIVO_SOSPECHA );
	k = 0;
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		if( (*p)->jugadores[i] != *j && (*p)->jugadores[i]->vivo == 1 ) {
			opciones[k].codigo = i + 1;
			sprintf( opciones[k].texto , "%s", (*p)->jugadores[i]->nombre );
			k++;
		}
	}
	k = Iu_preguntar("De quien sospechas?", opciones, k, (*j)->nombre ) - 1;
	if( c->tipo == TIPO_PRUEBA ) {
		Iu_imprimirMazo( (*p)->jugadores[k]->mano );
	} else {
		m = 0;
		maz = (*p)->jugadores[k]->mano;
		while( maz != NULL ) {
			if( maz->carta->valor == c->valor ) {
				opciones2[m].codigo = m + 1;
				sprintf( opciones2[m].texto , "%s", Iu_escribeCarta( maz->carta ) );
				m++;
			}
			maz = maz->siguiente;
		}
		if( m == 0 ) {
			maz = (*p)->jugadores[k]->mano;
			while( maz != NULL ) {
				opciones2[m].codigo = m + 1;
				sprintf( opciones2[m].texto , "%s", Iu_escribeCarta( maz->carta ) );
				m++;
				maz = maz->siguiente;
			}
		}
		m = Iu_preguntar("Que carta quieres mostrar?", opciones2, m , (*p)->jugadores[k]->nombre );
		printf("Muestra la carta %s", opciones2[m].texto);
	}
	
	return 0;
}

int Partida_atacar( struct partida ** p, struct jugador ** j ) {
	int k;
	int i;
 	struct opcion opciones[NUM_JUGADORES];
 	struct carta * c;

	Jugador_descartarConsciente( j , &c , MOTIVO_ATACAR );
	k = 0;
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		if( (*p)->jugadores[i] != *j && (*p)->jugadores[i]->vivo == 1 ) {
			opciones[k].codigo = i + 1;
			sprintf( opciones[k].texto , "%s", (*p)->jugadores[i]->nombre );
			k++;
		}
	}
	k = Iu_preguntar("A quien quieres atacar?", opciones, k, (*j)->nombre ) - 1;
	
	
	return 0;
}

int Partida_mision( struct partida ** p, struct jugador ** j ) {
	int mision;
	int aliado;
	int i;
	int k;
	char msg[200];
	struct carta * c;
	struct carta * c2;
	struct opcion opciones[NUM_MISIONES];
 	struct opcion opciones2[NUM_JUGADORES];
	
	for( i = 0 ; i < NUM_MISIONES ; ++i ) {
		opciones[i].codigo = i + 1;
		sprintf( opciones[i].texto , "Mision %i", i + 1 );
	}
	mision = Iu_preguntar("Que mision quieres realizar?", opciones, NUM_MISIONES, (*j)->nombre ) - 1;
	k = 0;
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		if( (*p)->jugadores[i] != *j ) {
			if( (*p)->jugadores[i]->vivo == 1 && Mazo_obtenerNumeroCartas( (*p)->jugadores[i]->mano ) > 2 ) {
				sprintf( msg , "Quieres colaborar con %s en la mision %i?" ,  (*j)->nombre, mision + 1 );
				opciones[0].codigo = 1;
				strcpy( opciones[0].texto, "Si" );	
				opciones[1].codigo = 2;
				strcpy( opciones[1].texto , "No" );	
				aliado = Iu_preguntar( msg , opciones,  2 , (*p)->jugadores[i]->nombre );
				if( aliado == 1 ) {
					opciones2[k].codigo = i + 1;
					strcpy( opciones2[k].texto , (*p)->jugadores[i]->nombre );
					++k;
				}
			}
		}
	}
	if( k == 0 ) {
		for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
			if( (*p)->jugadores[i]->vivo == 1 ) {
				Jugador_descartarConsciente( &((*p)->jugadores[i]), &c , MOTIVO_FALLO_MISION );
				Mesa_descartarCarta( &((*p)->mesa) , c );
			}
		}
		aliado = -1;
	} else if( k == 1 ){
		aliado = opciones2[0].codigo - 1;
	} else {
		sprintf( msg , "Cual de los voluntarios quieres llevar contigo?" );
		aliado = Iu_preguntar( msg , opciones2 , k , (*j)->nombre) - 1;
	}	
	if( aliado != -1 ) {
		printf( "La mision es :" );
		Iu_imprimirCarta( &(((*p)->mesa)->misiones[mision]) );
		Jugador_descartarConsciente( j , &c , MOTIVO_EXITO_MISION );
		Mazo_ponerCartaArriba( &((*p)->mesa->mazoMisiones[mision]), c );
		Jugador_descartarConsciente( &((*p)->jugadores[aliado]) , &c , MOTIVO_EXITO_MISION );		
		Mazo_ponerCartaArriba( &((*p)->mesa->mazoMisiones[mision]), c );
		Jugador_descartarConsciente( j , &c , MOTIVO_CONVERSACION );		
		Jugador_descartarConsciente( &((*p)->jugadores[aliado]) , &c2 , MOTIVO_CONVERSACION );		
		Jugador_cogerCarta( j , c2 );
		Jugador_cogerCarta(&((*p)->jugadores[aliado]) , c );		
	}
	
	return 0;
}

int Partida_pasar( struct partida ** p, struct jugador ** j ) {
	return 0;
}

int Partida_destruir( struct partida** p ) {
	int i;
	
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		Jugador_destruir( &((*p)->jugadores[i]) );
	}
	Mesa_destruir( &((*p)->mesa) );
	
	return 0;
}

int main() {
	struct partida *p;
	
	srand(time(NULL));
	Partida_crear(&p);
	Partida_preparar(&p);
	Partida_comenzar(&p);
	Partida_destruir( &p );

	exit(0);
}