#include <stdlib.h>

#ifndef cartaH
   #include "carta.h"
   #define cartaH
#endif

int Carta_crear( struct carta** c, int tipo, int valor ) {
	*c =(struct carta *)malloc( sizeof(struct carta) );
	(*c)->tipo = tipo;
	(*c)->valor = valor;

	return 0;
}