#include <stdlib.h>
#include <stdio.h>

#ifndef jugadorH
   #include "jugador.h"
   #define jugadorH
#endif

#ifndef iuH
   #include "iu.h"
   #define iuH
#endif

int Jugador_crear( struct jugador** j, int humano, int nombre ){
	*j = (struct jugador *)malloc( sizeof(struct jugador) );
	if( *j == NULL ) return -1;
	sprintf( (*j)->nombre, "Jugador_%i", nombre);
	(*j)->humano = humano;
	Mazo_crear( &((*j)->mano) );
	(*j)->bando = 0;
	(*j)->vivo = 1;
	
	return 0;
}

int Jugador_descartarAlAzar( struct jugador** j, struct carta** c ){
	int cartas;
	int carta;
	int error;
	struct mazo* m;
	
	error = 0;
	m = ((*j)->mano);
	cartas = Mazo_obtenerNumeroCartas( m );
	if( cartas > 0 ) {
		carta = rand() % cartas ;
		error = Mazo_robarCartaMedio( &m , carta, c );
	}
	
	return error;
}

int Jugador_descartarConsciente( struct jugador** j, struct carta** c, int motivo ) {
	int carta;
	int error;
	int valido;
	int numCartas;
	
	error = 0;
	valido = 0;
	numCartas = Mazo_obtenerNumeroCartas( (*j)->mano );
	if( numCartas > 1 ) {
		while( valido == 0 ) {
			valido = 1;
			switch( motivo ) {
				case MOTIVO_FALLO_MISION:
					carta = Iu_escogerCarta( "Que carta descartar por fallo al coordinar la mision?" , (*j)->mano, (*j)->nombre );
					error = Mazo_robarCartaMedio( &((*j)->mano) , carta - 1, c );
					break;
				case MOTIVO_EXITO_MISION:
					carta = Iu_escogerCarta( "Que carta aportas a la mision?" , (*j)->mano, (*j)->nombre );
					error = Mazo_robarCartaMedio( &((*j)->mano) , carta - 1, c );
					if( (*c)->tipo != TIPO_MECANICA && (*c)->tipo != TIPO_INVESTIGACION && (*c)->tipo != TIPO_COSA ) {
						printf( "No es una carta de utilidad.\n" );
						Mazo_ponerCartaArriba( &((*j)->mano) , *c );
						valido = 0;
					}
					break;
				case MOTIVO_CONVERSACION:
					carta = Iu_escogerCarta( "Que carta intercambias con tu acompanante?" , (*j)->mano, (*j)->nombre );
					error = Mazo_robarCartaMedio( &((*j)->mano) , carta - 1, c );
					break;
				case MOTIVO_REVISAR:
					carta = Iu_escogerCarta( "Que carta de especialista quieres jugar?",  (*j)->mano, (*j)->nombre );
					error = Mazo_robarCartaMedio( &((*j)->mano) , carta - 1, c );
					if( (*c)->tipo != TIPO_MECANIC0 && (*c)->tipo != TIPO_MEDICO && (*c)->tipo != TIPO_XENOINGENIERO && (*c)->tipo != TIPO_MANITAS ) {
						printf( "No es una carta de especialista.\n" );
						Mazo_ponerCartaArriba( &((*j)->mano) , *c );
						valido = 0;
					}
					break;
				case MOTIVO_SOSPECHA:
					carta = Iu_escogerCarta( "Que carta descartar para realizar la acusacion?" , (*j)->mano, (*j)->nombre );
					error = Mazo_robarCartaMedio( &((*j)->mano) , carta - 1, c );
					if( (*c)->tipo != TIPO_SOSPECHA && (*c)->tipo != TIPO_PRUEBA ) {
						printf( "No es una carta de sospecha.\n" );
						Mazo_ponerCartaArriba( &((*j)->mano) , *c );
						valido = 0;
					}
					break;
				case MOTIVO_ATACAR:
					carta = Iu_escogerCarta( "Que carta descartar para realizar el ataque?" , (*j)->mano, (*j)->nombre );
					error = Mazo_robarCartaMedio( &((*j)->mano) , carta - 1, c );
					if( (*c)->tipo != TIPO_MECANICA && (*c)->tipo != TIPO_INVESTIGACION && (*c)->tipo != TIPO_COSA ) {
						printf( "Esa carta no permite atacar.\n" );
						Mazo_ponerCartaArriba( &((*j)->mano) , *c );
						valido = 0;
					}
					break;	
			}
			if( valido == 1 && motivo != MOTIVO_HERIDA ) {			
				if( (*j)->bando == 1 ) {
					if( Mazo_numCartasTipo( (*j)->mano, TIPO_COSA ) == 0 ) {
						printf( "No puedes soltar tu ultima carta de la Cosa.\n" );
						Mazo_ponerCartaArriba( &((*j)->mano) , *c );
						valido = 0;
					}		
				}
			}			
		}			
	}
	
	return error;
}

int Jugador_cogerCarta( struct jugador** j, struct carta* c ) {
	int error;
	
	if( Jugador_numeroCartasMano( *j ) >= NUM_CARTAS_MANO ) return -1;
	error = Mazo_ponerCartaArriba( &((*j)->mano) , c );
	if( c->tipo == TIPO_COSA ) {
		(*j)->bando = 1;
	}
	
	return error;
}

int Jugador_numeroCartasMano( struct jugador* j ) {
	return Mazo_obtenerNumeroCartas( j->mano );
}

int Jugador_robar( struct jugador** j , struct mesa** m ) {
	int i;
	struct carta * c;
	int respuesta;
	char msg[90];
	struct opcion opciones[2];
	
	i = Mazo_obtenerNumeroCartas( (*j)->mano );
	while( i < NUM_CARTAS_MANO ) {
		Mesa_robarCarta( m , &c);
		if( c->tipo == TIPO_COSA || c->tipo == TIPO_XENOINGENIERO ) {
			if( (*j)->bando == 0 ) {
				printf("Descartada ");
				Iu_imprimirCarta( c );
				printf("\n");
				Mesa_descartarCarta( m, c );
			} else {
				opciones[0].codigo = 1;
				strcpy( opciones[0].texto, "Si" );	
				opciones[1].codigo = 2;
				strcpy( opciones[1].texto , "No" );	
				
				printf( "Mano de %s :\n", (*j)->nombre );
				Iu_imprimirMazo( (*j)->mano );
				if( c->tipo == TIPO_COSA ) {
					sprintf( msg , "\nHas robado una carta de tipo cosa con valor %i, descartar?", c->valor );
				} else {
					sprintf( msg , "\nHas robado una carta de Xenoingeniero, descartar?");	
				}
				respuesta = Iu_preguntar( msg , opciones, 2 , (*j)->nombre );
				if( respuesta == 1 ) {
					printf("Descartada ");
					Iu_imprimirCarta( c );
					printf("\n");
					Mesa_descartarCarta( m, c );
				} else {
					Mazo_ponerCartaArriba( &((*j)->mano), c );
					++i;
				}
			}
		} else {
			Mazo_ponerCartaArriba( &((*j)->mano), c );
			++i;
		}
	}
	
	return 0;
}

int Jugador_destruir( struct jugador** j ) {
	Mazo_destruir( &((*j)->mano) );
	//if( (*j)->nombre ) free( (*j)->nombre );
	if( *j ) free( *j );
	
	return 0;
}
