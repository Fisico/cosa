#include <stdlib.h>
#include <stdio.h>

#ifndef iuH
   #include "iu.h"
   #define iuH
#endif

int Iu_preguntar( char* texto, struct opcion opciones[] , int nOpciones, char* prompt ) {
	int respuesta;
	int final;
	int i;
	
	final = -1;
	printf( "%s\n", texto );
	for( i = 0 ; i  < nOpciones ; ++i ) {
		printf( "%i - %s\n", opciones[i].codigo, opciones[i].texto );
	}
	while( final == -1 ) {
		printf("%s>>>", prompt);
		scanf("%i", &respuesta);
	for( i = 0 ; i  < nOpciones ; ++i ) {
			if( opciones[i].codigo == respuesta ) {
				final = respuesta;
			}
		}
		if( final == -1 ) {
			printf( "opcion no valida. Debe estar entre las ofrecidas.\n" );
		}
	}
	
	return final;
}

int Iu_escogerCarta( char* texto, struct mazo* m , char* prompt  ) {
	struct mazo* aux;
	char * carta;
	int respuesta;
	int final;
	int i;
	
	final = -1;
	printf( "%s\n", texto );
	i = 1;
	if( ! Mazo_vacio( &m )  ) {
		aux = m;
		while( aux != NULL ) {
				carta = Iu_escribeCarta( aux->carta );
				printf( "%i - %s\n", i, carta );
				aux = aux->siguiente;
				++i;
		}
	}
	while( final == -1 ) {
		printf("%s>>>", prompt);
		scanf("%i", &respuesta);
		if( respuesta > 0 && respuesta <= i ) {
				final = respuesta;
		}
		if( final == -1 ) {
			printf( "opcion no valida. Debe estar entre las ofrecidas.\n" );
		}
	}
	
	return final;
}
	
char* Iu_escribeCarta( struct carta* c ) {
	char* carta;
	
	carta = (char *)malloc( sizeof(char) * 200 );
	 	
	switch( c->tipo ) {
		case TIPO_MECANICA:
			sprintf( carta, " {Mecanica,%d} ", c->valor);
			break;
		case TIPO_INVESTIGACION:
			sprintf( carta, " {Investigacion,%d} ", c->valor);
			break;
		case TIPO_COSA:
			sprintf( carta, " {Cosa,%d} ", c->valor);
			break;
		case TIPO_SOSPECHA:
			sprintf( carta, " {Sospecha,%d} ", c->valor);
			break;
		case TIPO_PRUEBA:
			sprintf( carta, " {Prueba sangre} ");
			break;
		case TIPO_MISION_MECANICA:
			sprintf( carta, " {Mision Mecanica} ");
			break;
		case TIPO_MISION_INVESTIGACION:
			sprintf( carta, " {Mision investigacion} ");
			break;
		case TIPO_MISION_COSA:
			sprintf( carta, " {Mision cosa} ");
			break;
		case TIPO_MECANIC0:
			sprintf( carta, " {Mecanico} ");
			break;
		case TIPO_MEDICO:
			sprintf( carta, " {Medico} ");
			break;
		case TIPO_XENOINGENIERO:
			sprintf( carta, " {Xenoingeniero} ");
			break;
		case TIPO_MANITAS:
			sprintf( carta, " {Manitas} ");
			break;
		}
		
		return carta;
}

void Iu_imprimirCarta( struct carta* c ) {
	char* carta;
	
	carta = Iu_escribeCarta( c );
	printf( "%s", carta );
	if( carta ) free( carta );
}

void Iu_imprimirMazo( struct mazo* m ) {
	struct mazo* aux;
	
	if( ! Mazo_vacio( &m )  ) {
		printf("<");
		aux = m;
		while( aux != NULL ) {
				Iu_imprimirCarta( aux->carta );
				aux = aux->siguiente;
		}
		printf(">");
	} else {
		printf("<Mazo vacio>");
	}
}

void Iu_imprimirMesa( struct mesa* m, int completo ) {
	int i;
	
	for( i = 0 ; i < NUM_MISIONES ; ++i ) {
		printf( "Mision %d : ", i+1 );
		Iu_imprimirCarta( &(m->misiones[i]) );
		printf("\t");
		Iu_imprimirMazo( m->mazoMisiones[i] );
		printf("\n");
	}
	printf("Mazo robo:\n");
	Iu_imprimirMazo( m->robo );
	printf("\n");
	printf("Mazo descartes:\n");
	Iu_imprimirMazo( m->descartes );
	printf("\n");
}

void Iu_imprimirJugador( struct jugador* j, int completo ) {
	printf( "Jugador: %s \t", j->nombre );
	if( completo == 1 ) {
		if( j->bando == 1 ) {
			printf( "(COSA)" );
		} else {
			printf("(HUMANO)");
		}
	}
	printf("\n");
	if( completo == 1 ) {
		printf("Mano: ");
		Iu_imprimirMazo( j->mano );
	} else {
		printf("Mano: %d \n", Mazo_obtenerNumeroCartas(j->mano) );
	}
	printf("\n");	
}

void Iu_imprimirPartida( struct partida* p, int completo, struct jugador* j ) {
	int i;
	
	printf("\n\nESTADO ACTUAL\n");
	Iu_imprimirMesa( p->mesa, completo );
	printf("\n");
	for( i = 0 ; i < NUM_JUGADORES ; ++i ) {
		if( completo == 1 || p->jugadores[i] == j ) {
			Iu_imprimirJugador( p->jugadores[i] , 1 );
		} else {
			Iu_imprimirJugador( p->jugadores[i] , 0 );			
		}
	}
}