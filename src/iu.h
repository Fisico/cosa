#ifndef cartaH
   #include "carta.h"
   #define cartaH
#endif

#ifndef mazoH
   #include "mazo.h"
   #define mazoH
#endif

#ifndef jugadorH
   #include "jugador.h"
   #define jugadorH
#endif

#ifndef partidaH
   #include "partida.h"
   #define partidaH
#endif

struct opcion {
	int codigo;
	char texto[200];
};

int Iu_preguntar( char* texto, struct opcion opciones[] , int nOpciones, char* prompt ) ;

int Iu_escogerCarta( char* texto, struct mazo* m , char* prompt  );

char* Iu_escribeCarta( struct carta* c );

void Iu_imprimirCarta( struct carta* c );

void Iu_imprimirMazo( struct mazo* m );

void Iu_imprimirJugador( struct jugador* j, int completo );

void Iu_imprimirMesa( struct mesa* m, int completo );

void Iu_imprimirPartida( struct partida* p, int completo, struct jugador* j );